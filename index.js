/* 
1. Copy the index.html and index.js from the discussion to the activity folder.
2. Listen to an event when the last name's input is changed.
3. Instead of anonymous functions for each of the event listener:
- Create a function that will update the span's contents based on the value of the first and last name input fields.
- struct the event listeners to use the created function.
4. Create a git repository named S47.
5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
6. Add the link in Boodle.

https://gitlab.com/tuitt/students/batch230/resources/s47/a1-instructions/-/blob/master/expected-output.gif
*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector('#span-full-name');
// const fullName = document.querySelector('.full-name');

// txtFirstName.addEventListener("keyup", (event) => {
//   spanFullName.innerHTML = txtFirstName.value;
// })

// txtLastName.addEventListener("keyup", (event) => {
//   spanFullName.innerHTML = txtLastName.value;
// })


const fullName = () => {
  let firstName = txtFirstName.value
  let lastName = txtLastName.value

  spanFullName.innerHTML = firstName + " " + lastName
}
txtLastName.addEventListener("keyup", fullName) + txtFirstName.addEventListener("keyup", fullName)



